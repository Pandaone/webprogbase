const WebSocketServer = require('ws').Server;

class WsServer {
    constructor(server) {
        this.connections = [];
        this.wsServer = new WebSocketServer({ server: server });
        this.wsServer.on('connection', (connection) => {
            this.addConnection(connection);
            connection.send("Response string at connection!");
            connection.on('message', (message) => {
                const dataStr = message.toString();
                console.log(`Got data: ${dataStr}`);
                connection.send(`Echo: ${dataStr}`);
            });
            connection.on('close', () => this.removeConnection(connection));
        });
    }
 
    addConnection(connection) { this.connections.push(connection); }
    removeConnection(connection) { this.connections = this.connections.filter(x => x !== connection); }
 
    notifyAll(text) {
        console.log(`WS> Notify all (${this.connections.length}) with: ${text}`);
        for (const connection of this.connections) { connection.send(`Notify: ${text}`); }
    }
 }; 

module.exports = WsServer;