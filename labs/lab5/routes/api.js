const playerRouter = require("./players.js");

const router = require('express').Router();

router.use("/players", playerRouter);

router.get('/', async function(req, res) {
    res.status(200).render('index.mst');
});

router.get('/about', async function(req, res) {
    res.status(200).render('about.mst');
});

module.exports = router;