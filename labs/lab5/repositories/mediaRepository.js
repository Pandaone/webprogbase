const config = require('../config.js');
const cloudinary = require('cloudinary').v2;
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

class MediaRepository {
    async addImageToCloud(data, folder){
        return new Promise((resolve, reject) => {
            cloudinary.uploader.upload_stream(
                    {resource_type: 'image', folder: folder}, 
                    (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                           resolve(result);
                        }
                    }).end(data);
        });
    }
};

module.exports = MediaRepository;