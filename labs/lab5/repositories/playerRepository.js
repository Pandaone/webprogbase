const PlayerModel = require("../models/players.js");
 
class PlayerRepository {
    async getPlayers(pattern) { 
        const playersDocs = await PlayerModel.find({name: {'$regex': new RegExp(pattern)}});
        const players = playersDocs.map(p => p.toObject());
        return players;
    }
 
    async getPlayerById(playerId) {
        const playerDoc = await PlayerModel.findOne({_id: playerId});
        if(!playerDoc){
            return;
        }
        const player = playerDoc.toObject();
        return player;
    }

    async getLastCreatedPlayers(number) {
        const playersDocs = await PlayerModel.find().sort({created: -1}).limit(number);
        const players = playersDocs.map(p => p.toObject());
        return players;
    }

    async addPlayer(playerModel) {
        await playerModel.save();
    }
 
    async updatePlayer(playerModel) { 
        const player = await this.getPlayerById(playerModel._id);
        if(player){
            player = playerModel;
            await player.save();
            return true;
        }
        return false;
    }
 
    async deletePlayer(playerId) {
        const res = await PlayerModel.deleteOne({ _id: playerId});
        if(res) return true;
        return false;
    }
};
 
module.exports = PlayerRepository;