require('dotenv').config();

const env = process.env.NODE_ENV;

const config = {
    dev: {
        app: {
            port: parseInt(process.env.DEV_APP_PORT) || 3000
        },
        db: {
            conn: process.env.DEV_CONN_STR || 'mongodb+srv://localhost',
            name: process.env.DEV_DB_NAME || 'db',
            params: process.env.DEV_DB_PARAMS || ''
        },
        cloudinary: {
            cloud_name: process.env.DEV_CLOUD_NAME,
            api_key: process.env.DEV_API_KEY,
            api_secret: process.env.DEV_API_SECRET
        }
    },
    test: {
        app: {
            port: parseInt(process.env.TEST_APP_PORT) || 3000
        },
        db: {
            conn: process.env.TEST_CONN_STR || 'mongodb+srv://localhost',
            name: process.env.TEST_DB_NAME || 'test',
            params: process.env.TEST_DB_PARAMS || ''
        },
        cloudinary: {
            cloud_name: process.env.TEST_CLOUD_NAME,
            api_key: process.env.TEST_API_KEY,
            api_secret: process.env.TEST_API_SECRET
        }
    },
    prod: {
        app: {
            port: process.env.PORT || 3000
        },
        db: {
            conn: process.env.PROD_CONN_STR || 'mongodb+srv://localhost',
            name: process.env.PROD_DB_NAME || 'test',
            params: process.env.PROD_DB_PARAMS || ''
        },
        cloudinary: {
            cloud_name: process.env.PROD_CLOUD_NAME,
            api_key: process.env.PROD_API_KEY,
            api_secret: process.env.PROD_API_SECRET
        }
    }   
};

module.exports = config[env];