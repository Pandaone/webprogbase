const config = require('./config.js');
const http = require('http');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const mustache = require('mustache-express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const port = config.app.port;

const dbUrl = `${config.db.conn}${config.db.name}${config.db.params}`;
const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};

const app = express();
const server = http.createServer(app);

const WsServer = require('./modules/websocketserver.js');
const wsServer = new WsServer(server);

app.use(morgan('dev'));

app.use(bodyParser.json({extended: true}));

app.use(express.static(path.join(__dirname, './public')));

const busboy = require('busboy-body-parser');
const optionsBusboy = {
   limit: '5mb',
   multi: false,
};
app.use(busboy(optionsBusboy));

const apiRouter = require('./routes/api.js');
app.use('', apiRouter);

const viewsDir = path.join(__dirname, 'public/views');
app.engine('mst', mustache(path.join(viewsDir, 'partials')));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.get('/', function(req, res) {
    res.render('index.mst');
});

app.post('/messages', (req, res) => {
    const message = req.body.name;
    wsServer.notifyAll(message);
});

server.listen(port, async function () {
    console.log(`Server ready`);
    await mongoose.connect(dbUrl, connectOptions);
    console.log('Mongo database connected');
});