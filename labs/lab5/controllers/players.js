const playerRepository = require("../repositories/playerRepository.js");
const playerRep = new playerRepository();

const mediaRepository = require("../repositories/mediaRepository.js");
const mediaRep = new mediaRepository();

const PlayerModel = require("../models/players.js");

const playerSymbol = Symbol("player");
const photoSymbol = Symbol("photo");

const fetch = require('node-fetch');

async function notifyAll(text){
    const fetchOptions = {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            //"Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify({"name": text}), // body data type must match "Content-Type"
    };
    
    fetch("http://localhost:3000/messages", fetchOptions)
    .catch(
        error => console.log(error) // Handle the error response object
    );
}

module.exports = {
    async getPlayer(req, res, next){
        const name = req.body.name;
        const position = req.body.position;
        const number = parseInt(req.body.number);
        const age = new Date(req.body.age);

        if(age == "Invalid Date" || !number || !position || !name){
            res.sendStatus(204);
            return;
        }
        const player = new PlayerModel({
            name: name,
            position: position,
            age: age,
            number: number
        });
        req[playerSymbol] = player;
        req[photoSymbol] = req.files.photo;

        next();
    },

    async getAllPlayers(req, res) {
        try{
            const text = req.query.text.toLowerCase();
            const allPlayers = await playerRep.getPlayers(text);

            let players = [];
            const page = parseInt(req.query.page);
            const per_page = parseInt(req.query.per_page);
            for(let i = (page - 1) * per_page; i < page * per_page; i++){
                if(!allPlayers[i]){
                    break;
                }
                players.push(allPlayers[i]);
            }

            let pages;
            if(players.length === 0){
                pages = {"cur_page": 1};
            }
            else{
                const max_page = Math.ceil(allPlayers.length / per_page);
                let next_page = null;
                if(max_page > page){
                    next_page = page + 1;
                }
                let prev_page = null;
                if(page > 1){
                    prev_page = page - 1;
                }
                pages = {
                    "prev_page": prev_page,
                    "cur_page": page,
                    "next_page": next_page
                };
            }
            res.status(200).render('players.mst', {
                players,
                pages,
                text
            });
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getLastCreatedPlayers(req, res){
        try{
            const number = 5; // number of last created players we want to get
            const players = await playerRep.getLastCreatedPlayers(number);
            for(const player of players){
                player.created = player.created.toISOString().substring(0, 10);
            }
            res.send(players);
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getPlayerById(req, res) {
        try{
            const playerId = req.params.id;
            const player = await playerRep.getPlayerById(playerId);
            if(player){
                player.age = player.age.toISOString().substring(0, 10);
                
                res.status(200).render("player.mst", {
                    player
                });
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(500).send(response);
        }
    },

    async createPlayer(req, res) {
        try{
            const player = req[playerSymbol];
            const photo = req[photoSymbol];
            if(photo){
                const res = await mediaRep.addImageToCloud(photo.data, "lab5");
                player.photo = res.secure_url;
            }
            await Promise.all([notifyAll(player.name), player.save()]);
            res.status(201).send(player);
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async putPlayer(req, res){
        try{
            const player = req[playerSymbol];
            const success = await playerRep.updatePlayer(player);
            if(success){
                res.redirect(`/players/${player._id}`);
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async deletePlayer(req, res){
        try{
            const playerId = req.body.id;
            const player = await playerRep.deletePlayer(playerId);
            if(player){
                res.redirect("/players?page=1&per_page=4&text=");
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    }

};