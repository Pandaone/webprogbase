const mongoose = require('mongoose');

const PlayerShema = new mongoose.Schema({
    name: {type: String, required: true},
    photo: {type: String, default: "https://res.cloudinary.com/duzykfess/image/upload/v1619377011/lab5/player_w7wyre.png"},
    position: {type: String, required: true},
    age: {type: Date, required: true},
    number: {type: Number, required: true},
    created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Players', PlayerShema, 'Players');