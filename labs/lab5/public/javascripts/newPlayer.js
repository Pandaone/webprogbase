async function createPlayer(event){
    event.preventDefault();

    const form = $('form')[0]
    const formData = new FormData(form);

    const fetchOptions = {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        /*headers: {
            //"Content-Type": "application/json; charset=utf-8",
            //"Content-Type": "application/x-www-form-urlencoded",
        },*/
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: formData, // body data type must match "Content-Type"
    };

    fetch('/players', fetchOptions)  // POST request
    //.then(res => res.json())
    .catch(
        error => console.log(error) // Handle the error response object
    );
    form.reset();
    await loadLastCreated();
}

async function loadLastCreated(){
    Promise.all([
        fetch("/views/templates/lastCreated.mst").then(x => x.text()),
        fetch("/players/last_created").then(x => x.json()),
    ])
    .then(([templateStr, itemsData]) => {
        const dataObject = {players: itemsData};
        const renderedHtmlStr = Mustache.render(templateStr, dataObject);
        return renderedHtmlStr;
    })
    .then(htmlStr => {
        const appEl = document.getElementById('players');
        appEl.innerHTML = htmlStr;
    })
    .catch(err => console.error(err));
}

window.onload = async function() {
    setTimeout(() => {
        loadLastCreated(); 
    }, 1000);
};