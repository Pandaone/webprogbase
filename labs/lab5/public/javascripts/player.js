function deletePlayer(id){
    const xhttp = new XMLHttpRequest();
    xhttp.addEventListener('load', async function() {
        setTimeout(() => { 
            window.location.replace("/players?page=1&per_page=4&text=");
        }, 3000);
        
    });
    xhttp.open("POST", "/players/delete", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("id=" + id);

    const modal_footer = $(".modal-footer");
    modal_footer.find(".btn-primary").remove();
    modal_footer.append('<div class="spinner-border text-primary"></div>');
}