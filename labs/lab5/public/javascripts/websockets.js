const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);
 
connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message', (message) => {
    console.log(`message-${message.data}`)
});
connection.addEventListener('close', () => console.log(`Disconnected from ws server`));