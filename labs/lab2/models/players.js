/**
 * @typedef Player
 * @property {integer} id
 * @property {string} name.required - player name
 * @property {string} position
 * @property {string} age - date of birth
 * @property {integer} number
 */
class Player {

    constructor(id, name, position , age, number) {
        this.id = id;  // number
        this.name = name;  // string
        this.position = position; //string
        this.age = age; // ISO 8601
        this.number = number; // number
    }
 };
 
 module.exports = Player; 