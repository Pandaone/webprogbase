const router = require('express').Router();

const bodyParser = require('body-parser');

const playerController = require('../controllers/players.js');

const urlencodedParser = bodyParser.json();

/**
 * Get player by id
 * @route GET /api/players/{id}
 * @group Players - player operations
 * @param {integer} id.path.required - id of the Player - eg: 1
 * @returns {Player.model} 200 - Player object
 * @returns {Error} 404 - Player not found
 */
router.get("/:id(\\d+)", playerController.getPlayerById);

/**
 * Get all players
 * @route Get /api/players
 * @group Players - player operation
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Player>} 200 - All players
 * @returns {Error} 404 - No players
 */
router.get("", playerController.getPlayers);

/**
 * Create new player
 * @route POST /api/players
 * @group Players - player operation
 * @param {Player.model} player.body.required - new Player
 * @returns {Player.model} 201 - Player created
 * @returns {Error} 400 - Bad request
 */
router.post("", urlencodedParser, playerController.getPlayer, playerController.createPlayer);

/**
 * Put player
 * @route PUT /api/players
 * @group Players - player operations
 * @param {Player.model} player.body.required - new Player object
 * @returns {Player.model} 200 - changed Player object
 * @returns {Error} 404 - Player not found
 */
router.put("", urlencodedParser, playerController.getPlayer, playerController.putPlayer);

/**
 * Delete player
 * @route DELETE /api/players/{id}
 * @group Players - player operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - deleted User object
 * @returns {Error} 404 - User not found
 */
router.delete("/:id(\\d+)", playerController.deletePlayer);

module.exports = router;