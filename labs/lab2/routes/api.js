const userRouter = require("./users.js");
const playerRouter = require("./players.js");
const mediaRouter = require("./media.js");

const router = require('express').Router();

router.use("/users", userRouter)
    .use("/players", playerRouter)
    .use("/media", mediaRouter);


module.exports = router;