const userRepository = require("../repositories/userRepository.js");
const userRep = new userRepository("./data/users.json");

const User = require("../models/users.js");

module.exports = {
    getUsers(req, res) {
        const users = userRep.getUsers();
        let response = [];
        const page = req.query.page;
        const per_page = req.query.per_page;
        for(let i = (page - 1) * per_page; i < page * per_page; i++){
            if(!users[i]){
                break;
            }
            response.push(users[i]);
        }
        if(response.length === 0){
            const responseErr = {
                "Error": "Not found"
            };
            res.status(404).send(responseErr);
            return;
        }
        res.send(response);
    },

    getUserById(req, res) {
        const userId = req.params.id;
        const user = userRep.getUserById(parseInt(userId));
        if(user){
            res.send(user);
        }
        else{
            const response = {
                "Error": "User not found"
            };
            res.status(404).send(response);
        }
    },

    createUser(req, res) {
        const login = req.body.login;
        if(!login){
            const response = {
                "Error": "Bad request"
            };
            res.status(400).send(response);
            return;
        }
        const fullname = req.body.fullname;
        if(!fullname){
            const response = {
                "Error": "Bad request"
            };
            res.status(400).send(response);
            return;
        }
        let role = parseInt(req.body.role);
        if(!role){
            role = 0;
        }
        let registeredAt = new Date(req.body.registeredAt);
        if(registeredAt == "Invalid Date"){
            registeredAt = new Date("2000-02-02");
        }
        registeredAt = registeredAt.toISOString();
        let avaUrl = req.body.avaUrl;
        if(!avaUrl){
            avaUrl = "https://vscode.ru/wp-content/uploads/2017/11/hello-world.jpg";
        }
        const user = new User(0, login, fullname, role, registeredAt, 
            avaUrl, (req.body.isEnabled === "true"));
        userRep.addUser(user);
        res.status(201).send(user);
    }
};
