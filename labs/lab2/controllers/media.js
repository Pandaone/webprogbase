const path = "./data/media/";

const mediaRepository = require("../repositories/mediaRepositiry.js");
const mediaRep = new mediaRepository(path);

module.exports = {
    postImage(req, res){
        const fileParams = req.files.image.name.split(".");
        const fileObj = mediaRep.postImage(fileParams[0], req.files.image.data, fileParams[1]);  
        res.send(fileObj);
    },

    getImageById(req, res){      
        const id = parseInt(req.params.id);
        const fileObj = mediaRep.getImageById(id);
        if(!fileObj){
            const responseErr = {
                "Error": "Not found"
            };
            res.status(404).send(responseErr);
            return;
        }
        res.download(path + fileObj.name);
    }
};