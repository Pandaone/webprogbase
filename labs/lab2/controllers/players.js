const playerRepository = require("../repositories/playerRepository.js");
const playerRep = new playerRepository("./data/players.json");

const Player = require("../models/players.js");

const playerSymbol = Symbol("player");

module.exports = {
    getPlayer(req, res, next){
        let id = parseInt(req.body.id);
        if(!id){
            id = 0;
        }
        const name = req.body.name;
        if(!name){
            const response = {
                "Error": "Bad request"
            };
            res.status(400).send(response);
            return;
        }
        let position = req.body.position;
        if(!position){
            position = "midfielder";
        }
        let age = new Date(req.body.age);
        if(age == "Invalid Date"){
            age = new Date("2000-02-02");
        }
        age = age.toISOString();
        let number = parseInt(req.body.number);
        if(!number){
            number = 0;
        }
        const player = new Player(id, name, position, age, number);
        req[playerSymbol] = player;
        next();
    },

    getPlayers(req, res) {
        const players = playerRep.getPlayers();
        let response = [];
        const page = req.query.page;
        const per_page = req.query.per_page;
        for(let i = (page - 1) * per_page; i < page * per_page; i++){
            if(!players[i]){
                break;
            }
            response.push(players[i]);
        }
        if(response.length === 0){
            const responseErr = {
                "Error": "Not found"
            };
            res.status(404).send(responseErr);
            return;
        }
        res.send(response);
    },

    getPlayerById(req, res) {
        const playerId = parseInt(req.params.id);
        const player = playerRep.getPlayerById(playerId);
        if(player){
            res.send(player);
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    },

    createPlayer(req, res) {
        const player = req[playerSymbol];
        playerRep.addPlayer(player);
        res.status(201).send(player);
    },

    putPlayer(req, res){
        const player = req[playerSymbol];
        const success = playerRep.updatePlayer(player);
        if(success){
            res.send(player);
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    },

    deletePlayer(req, res){
        const playerId = parseInt(req.params.id);
        const player = playerRep.deletePlayer(playerId);
        if(player){
            res.send(player[0]);
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    }
};