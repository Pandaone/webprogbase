const Player = require("../models/players.js");
const JsonStorage = require("../jsonStorage.js");
 
class PlayerRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getPlayers() { 
        const players = this.storage.readItems();
        let playersArr = [];
        for(const player of players){
            if(typeof player.id !== 'undefined' && typeof player.name !== 'undefined' && 
            typeof player.position !== 'undefined' && typeof player.age !== 'undefined' && 
            typeof player.number !== 'undefined'){
                const newPLayer = new Player(player.id, player.name, player.position, player.age, player.number);
                playersArr.push(newPLayer); 
            }
        }
        return playersArr;
    }
 
    getPlayerById(playerId) {
        const players = this.getPlayers();
        for (const player of players) {
            if (player.id === playerId) {
                return player;
            }
        }
        return null;
    }

    addPlayer(playerModel) {
        const items = this.getPlayers();
        const id = this.storage.nextId;
        playerModel.id = id;
        items.push(playerModel);
        this.storage.writeItems(items);
        this.storage.incrementNextId();
        return id;
    }
 
    updatePlayer(playerModel) { 
        const players = this.getPlayers();
        for(let player of players){
            if(player.id === playerModel.id){
                player.name = playerModel.name;
                player.position = playerModel.position;
                player.age = playerModel.age;
                player.number = playerModel.number;
                player.titles = playerModel.titles;
                this.storage.writeItems(players);
                return true;
            }
        }
        return false;
    }
 
    deletePlayer(playerId) {
        let players = this.getPlayers();
        for(let i = 0; i < players.length; i++){
            if(players[i].id === playerId){
                const player = players.splice(i, 1);
                this.storage.writeItems(players);
                return player;
            }
        }
        return false;
    }
};
 
module.exports = PlayerRepository;