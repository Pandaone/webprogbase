const fs = require("fs");

const JsonStorage = require("../jsonStorage.js");

class mediaRepository {
    constructor(path){
        this.path = path;
        this.storage = new JsonStorage(path + "/media.json");
    }

    postImage(name, data, type){
        let filename = "";
        if(fs.existsSync(this.path + `${name}.${type}`)){
            filename = uploadImageCopy(this.path, name, data, type, 1);
        }
        else{
            filename = `${name}.${type}`;
            fs.writeFileSync(this.path + filename, data);
        }
        const id = this.storage.nextId;
        const fileObj = {
            "id": id,
            "name": filename
        };
        const files = this.storage.readItems();
        files.push(fileObj);
        this.storage.writeItems(files);
        this.storage.incrementNextId();
        return fileObj;
    }

    getImageById(fileId){
        const files = this.storage.readItems();
        for(const fileObj of files){
            if(fileObj.id === fileId){
                return fileObj;
            }
        }
        return false;
    }
}

function uploadImageCopy(path, name, data, type, count){
    if(fs.existsSync(path + `${name}${count}.${type}`)){
        return uploadImageCopy(path, name, data, type, count + 1);
    }
    else{
        fs.writeFileSync(path + `${name}${count}.${type}`, data);
        return `${name}${count}.${type}`;
    }
}

module.exports = mediaRepository;