const port = 3000;

const express = require('express');

const app = express();

const morgan = require('morgan');
app.use(morgan('dev'));

app.use(express.static('public'));

const busboy = require('busboy-body-parser');
const optionsBusboy = {
   limit: '5mb',
   multi: false,
};
app.use(busboy(optionsBusboy));

const apiRouter = require('./routes/api.js');
app.use('', apiRouter);

const mustache = require('mustache-express');
const path = require('path');

const viewsDir = path.join(__dirname, 'public/views');
app.engine('mst', mustache(path.join(viewsDir, 'partials')));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.get('/', function(req, res) {
    res.render('index.mst');
});

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
const options = {
    swaggerDefinition: {
        info: {
            description: 'Main Page',
            title: 'CRUD menu',
            version: '1.0.0',
        },
        host: 'localhost:' + port.toString(),
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.listen(port, function() { console.log('Server is ready'); });
