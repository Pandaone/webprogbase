/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname.required - user fullname
 * @property {integer} role
 * @property {string} registeredAt - date of registrtion
 * @property {string} avaUrl - image url
 * @property {boolean} isEnabled
 */
class User {

    constructor(id, login, fullname, role , registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role; // 0 or 1
        this.registeredAt = registeredAt; // ISO 8601
        this.avaUrl = avaUrl; // URL string
        this.isEnabled = isEnabled; // bool
    }
 };
 
 module.exports = User; 