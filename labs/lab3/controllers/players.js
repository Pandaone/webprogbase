const playerRepository = require("../repositories/playerRepository.js");
const playerRep = new playerRepository("./data/players.json");

const Player = require("../models/players.js");

const playerSymbol = Symbol("player");

const path = "./data/media.json";
const path_to_files = "./public/images/playersImages/";

const mediaRepository = require("../repositories/mediaRepositiry.js");
const mediaRep = new mediaRepository(path, path_to_files);

module.exports = {
    getPlayer(req, res, next){
        let id = parseInt(req.body.id);
        if(!id){
            id = 0;
        }

        const name = req.body.name;
        let position = req.body.position;
        let number = parseInt(req.body.number);
        let age = new Date(req.body.age);

        if(age == "Invalid Date" || !number || !position || !name){
            res.sendStatus(204);
            return;
        }
        age = age.toISOString().substring(0, 10);
        const player = new Player(id, name, position, age, number);
        req[playerSymbol] = player;
        next();
    },

    getAllPlayers(req, res) {
        let allPlayers = playerRep.getPlayers();
        let players = [];
        const page = parseInt(req.query.page);
        const per_page = parseInt(req.query.per_page);
        const text = req.query.text.toLowerCase();

        allPlayers = allPlayers.filter(p => p.name.toLowerCase().includes(text));
        for(let i = (page - 1) * per_page; i < page * per_page; i++){
            if(!allPlayers[i]){
                break;
            }
            players.push(allPlayers[i]);
        }
        
        const max_page = Math.ceil(allPlayers.length / per_page);
        let next_page = null;
        if(max_page > page){
            next_page = page + 1;
        }
        let prev_page = null;
        if(page > 1){
            prev_page = page - 1;
        }
        const pages = {
            "prev_page": prev_page,
            "cur_page": page,
            "next_page": next_page
        };
        res.status(200).render('players.mst', {
            players,
            pages,
            text,
        });
    },

    getPlayerById(req, res) {
        const playerId = parseInt(req.params.id);
        const player = playerRep.getPlayerById(playerId);
        if(player){
            const images = mediaRep.getImagesByPlayerId(playerId);
            
            res.status(200).render("player.mst", {
                player,
                images,
            });
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    },

    createPlayer(req, res) {
        const player = req[playerSymbol];
        req.body.id = playerRep.addPlayer(player);
        res.status(201);
        res.redirect(`/players/${player.id}`);
    },

    putPlayer(req, res){
        const player = req[playerSymbol];
        const success = playerRep.updatePlayer(player);
        if(success){
            res.send(player);
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    },

    deletePlayer(req, res){
        const playerId = parseInt(req.body.id);
        const player = playerRep.deletePlayer(playerId);
        if(player){
            res.redirect("/players?page=1&per_page=5&text=");
        }
        else{
            const response = {
                "Error": "Not found"
            };
            res.status(404).send(response);
        }
    },
};