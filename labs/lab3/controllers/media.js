const path = "./data/media.json";
const path_to_files = "./public/images/playersImages/";

const mediaRepository = require("../repositories/mediaRepositiry.js");
const mediaRep = new mediaRepository(path, path_to_files);

module.exports = {
    postPlayerImage(req, res){
        const player_id = parseInt(req.body.id);
        const image = req.files.image;
        if(image){
            const filename = image.name;
            mediaRep.postImage(filename, image.data, player_id); 
            res.status(201);
            res.redirect(`/players/${player_id}`);
        }
        else{
            res.sendStatus(204);
        }
    },

    getImageById(req, res){      
        const id = parseInt(req.params.id);
        const fileObj = mediaRep.getImageById(id);
        if(!fileObj){
            const responseErr = {
                "Error": "Not found"
            };
            res.status(404).send(responseErr);
            return;
        }
        res.download(path_to_files + fileObj.name);
    }
};