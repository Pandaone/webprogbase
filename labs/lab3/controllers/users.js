const userRepository = require("../repositories/userRepository.js");
const userRep = new userRepository("./data/users.json");

const User = require("../models/users.js");

module.exports = {
    getUsers(req, res) {
        const allUsers = userRep.getUsers();
        let users = [];
        const page = parseInt(req.query.page);
        const per_page = parseInt(req.query.per_page);

        for(let i = (page - 1) * per_page; i < page * per_page; i++){
            if(!allUsers[i]){
                break;
            }
            users.push(allUsers[i]);
        }
        if(users.length === 0){
            const responseErr = {
                "Error": "Not found"
            };
            res.status(404).send(responseErr);
            return;
        }

        const max_page = Math.ceil(allUsers.length / per_page);
        let next_page = null;
        if(max_page > page){
            next_page = page + 1;
        }
        let prev_page = null;
        if(page > 1){
            prev_page = page - 1;
        }
        const pages = {
            "prev_page": prev_page,
            "cur_page": page,
            "next_page": next_page
        };
        res.render('users.mst', {
            users,
            pages
        });
    },

    getUserById(req, res) {
        const userId = req.params.id;
        const user = userRep.getUserById(parseInt(userId));
        if(user){
            res.status(200).render("user.mst", {
                user,
            });
        }
        else{
            const response = {
                "Error": "User not found"
            };
            res.status(404).send(response);
        }
    },

    createUser(req, res) {
        const login = req.body.login;
        const fullname = req.body.fullname;
        const role = parseInt(req.body.role);
        let registeredAt = new Date(req.body.registeredAt);
        registeredAt = registeredAt.toISOString().substring(0, 10);
        const avaUrl = req.body.avaUrl;
        if(registeredAt == "Invalid Date" || !login || !fullname || !role || !avaUrl){
            const response = {
                "Error": "Bad request"
            };
            res.status(400).send(response);
            return;
        }
        const user = new User(0, login, fullname, role, registeredAt, 
            avaUrl, (req.body.isEnabled === "true"));
        userRep.addUser(user);
        res.status(201).send(user);
    }
};
