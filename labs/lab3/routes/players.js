const router = require('express').Router();

const bodyParser = require('body-parser');

const playerController = require('../controllers/players.js');

const urlencodedParser = bodyParser.urlencoded({ extended: false });

/**
 * Get player by id
 * @route GET /players/{id}
 * @group Players - player operations
 * @param {integer} id.path.required - id of the Player - eg: 1
 * @returns {Player.model} 200 - Player object
 * @returns {Error} 404 - Player not found
 */
router.get("/:id(\\d+)", playerController.getPlayerById);

/**
 * Get all players
 * @route Get /api/players
 * @group Players - player operation
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Player>} 200 - All players
 * @returns {Error} 404 - No players
 */
router.get("", playerController.getAllPlayers);

/**
 * Create new player
 * @route POST /players
 * @group Players - player operation
 * @param {Player.model} player.body.required - new Player
 * @returns {Player.model} 201 - Player created
 * @returns {Error} 400 - Bad request
 */
router.post("", urlencodedParser, playerController.getPlayer, playerController.createPlayer);

/**
 * Put player
 * @route PUT /players
 * @group Players - player operations
 * @param {Player.model} player.body.required - new Player object
 * @returns {Player.model} 200 - changed Player object
 * @returns {Error} 404 - Player not found
 */
router.put("", playerController.getPlayer, playerController.putPlayer);

/**
 * Delete player
 * @route DELETE /players/{id}
 * @group Players - player operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - deleted User object
 * @returns {Error} 404 - User not found
 */
router.post("/delete", urlencodedParser, playerController.deletePlayer);

router.get("/new", function(req, res){
    res.render('new_player.mst');
});

module.exports = router;