const router = require('express').Router();

const bodyParser = require('body-parser');

const userController = require('../controllers/users.js');

/**
 * Get user by id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get("", userController.getUsers);

/**
 * Get all users
 * @route Get /api/users
 * @group Users - user operation
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<User>} 200 - All users
 * @returns {Error} 404 - No users
 */
router.get("/:id(\\d+)", userController.getUserById);

/**
 * Create new user
 * @route POST /api/users
 * @group Users - user operation
 * @param {User.model} user.body.required - new User
 * @returns {User.model} 201 - User created
 * @returns {Error} 400 - Bad request
 */
router.post("", bodyParser.urlencoded({ extended: false }), userController.createUser);


module.exports = router;