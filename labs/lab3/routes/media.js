const router = require('express').Router();

const mediaController = require('../controllers/media.js');

/**
 * Upload photo
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {fileObj} 200 - added image
 */
router.post("/player", mediaController.postPlayerImage);

/**
 * Get image by id
 * @route GET /api/media/{id}
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {integer} id.path.required - id of the Image - eg: 1
 * @returns {file} 200 - Image
 * @returns {Error} 404 - Image not found
 */
router.get("/:id(\\d+)", mediaController.getImageById);

module.exports = router;