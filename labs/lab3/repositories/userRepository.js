const User = require("../models/users.js");
const JsonStorage = require("../jsonStorage.js");
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() { 
        const users = this.storage.readItems();
        let usersArr = [];
        for(let user of users){
            if(typeof user.id !== 'undefined' && typeof user.login !== 'undefined' && typeof user.fullname && 
            typeof user.role !== 'undefined' && typeof user.registeredAt !== 'undefined' && 
            typeof user.avaUrl !== 'undefined' && typeof user.isEnabled !== 'undefined'){
                const newUser = new User(user.id, user.login, user.fullname, user.role, user.registeredAt, user.avaUrl, user.isEnabled);
                usersArr.push(newUser); 
            }
        }
        return usersArr;
    }
 
    getUserById(userId) {
        const items = this.getUsers();
        for (const item of items) {
            if (item.id === userId) {
                return item;
            }
        }
        return null;
    }

    addUser(userModel) {
        const items = this.getUsers();
        const id = this.storage.nextId;
        userModel.id = id;
        items.push(userModel);
        this.storage.writeItems(items);
        this.storage.incrementNextId();
        return id;
    }
 
    updateUser(userModel) {
        const users = this.getUsers();
        for(const user of users){
            if(user.id === userModel.id){
                user.login = userModel.login;
                user.fullname = userModel.fullname;
                user.role = userModel.role;
                user.avaUrl = userModel.avaUrl;
                user.registeredAt = userModel.registeredAt;
                user.isEnabled = userModel.isEnabled;
                this.storage.writeItems(users);
                return true;
            }
        }
        return false;
    }
 
    deleteUser(userId) {
        const users = this.getUsers();
        const filteredUsers = users.filter(x => x.id !== userId);
        if(users.length !== filteredUsers.length){
            this.storage.writeItems(filteredUsers);
            return true;
        }
        return false;
    }
};
 
module.exports = UserRepository;