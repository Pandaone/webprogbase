const fs = require("fs");

const JsonStorage = require("../jsonStorage.js");

class mediaRepository {
    constructor(path, path_to_files){
        this.path = path;
        this.path_to_files = path_to_files;
        this.storage = new JsonStorage(path);
    }

    postImage(name, data, player_id){
        const tmp = name.split(".");
        const fn = tmp[0];
        const fext = tmp[1];
        let filename = "";
        if(fs.existsSync(this.path_to_files + `${fn}.${fext}`)){
            filename = uploadImageCopy(this.path_to_files, fn, data, fext, 1);
        }
        else{
            filename = `${fn}.${fext}`;
            fs.writeFileSync(this.path_to_files + filename, data);
        }
        const id = this.storage.nextId;
        const fileObj = {
            "id": id,
            "player_id": player_id,
            "name": filename
        };
        const files = this.storage.readItems();
        files.push(fileObj);
        this.storage.writeItems(files);
        this.storage.incrementNextId();
        return filename;
    }

    getImageById(fileId){
        const files = this.storage.readItems();
        for(const fileObj of files){
            if(fileObj.id === fileId){
                return fileObj;
            }
        }
        return false;
    }

    getImagesByPlayerId(playerId){
        const files = this.storage.readItems();
        const images = [];
        for(const fileObj of files){
            if(fileObj.player_id === playerId){
                images.push(fileObj);
            }
        }
        return images;
    }
}

function uploadImageCopy(path, name, data, type, count){
    if(fs.existsSync(path + `${name}${count}.${type}`)){
        return uploadImageCopy(path, name, data, type, count + 1);
    }
    else{
        fs.writeFileSync(path + `${name}${count}.${type}`, data);
        return `${name}${count}.${type}`;
    }
}

module.exports = mediaRepository;