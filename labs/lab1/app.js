const UserRepository = require("./repositories/userRepository.js");
const userRep = new UserRepository("./data/users.json");

const PlayerRepository = require("./repositories/playerRepository");
const playerRep = new PlayerRepository("./data/players.json");

const Player = require("./models/players.js");

const readline = require("readline-sync");

try{
    while (true) {
        const input = readline.question("Enter  your command:\n");
        console.clear();

        if (input === "get/users") {
            const users = userRep.getUsers();
            for(const user of users){
                console.log(user.fullname);
            }
            console.log("");
        }
        else if (input.substring(0, 9) === "get/user/" && input.split("/").length === 3){
            const splitText = input.split("/");
            const id = parseInt(splitText[2]);
            if(id){
                const user = userRep.getUserById(id);
                if(user){
                    console.log("Fullname - " + user.fullname);
                    console.log("Login - " + user.login);
                    console.log("Registered at: " + user.registeredAt);
                    console.log("");
                }
                else{
                    console.log("No user with such id");
                }
            }
            else{
                console.log("Id should be a number");
            }
        }
        else if(input.substring(0, 12) === "delete/user/" && input.split("/").length === 3){
            const splitText = input.split("/");
            const id = parseInt(splitText[2]);
            if(id){
                const success = userRep.deleteUser(id);
                if(success){
                    console.log("User was deleted");
                }
                else{
                    console.log("No user with such id");
                }
            }
            else{
                console.log("Id should be a number");
            }
        }
        else if (input === "get/players") {
            const players = playerRep.getPlayers();
            for(const player of players){
                console.log(`${player.name}(${player.number})\n${player.position}\n`);
            }
        }
        else if (input.substring(0, 11) === "get/player/" && input.split("/").length === 3){
            const splitText = input.split("/");
            const id = parseInt(splitText[2]);
            if(id){
                const player = playerRep.getPlayerById(id);
                if(player){
                    console.log(`${player.name}(${player.number})\n${player.position}`);
                    console.log("Titles won - " + player.titles);
                    console.log("Date of birth - " + player.age);
                    console.log("");
                }
                else{
                    console.log("No player with such id");
                }
            }
            else{
                console.log("Id should be a number");
            }
        }
        else if(input.substring(0, 14) === "delete/player/" && input.split("/").length === 3){
            const splitText = input.split("/");
            const id = parseInt(splitText[2]);
            if(id){
                const success = playerRep.deletePlayer(id);
                if(success){
                    console.log("Player was deleted");
                }
                else{
                    console.log("No player with such id");
                }
            }
            else{
                console.log("Id should be a number");
            }
        }
        else if(input.substring(0, 14) === "update/player/" && input.split("/").length === 3){
            const splitText = input.split("/");
            const id = parseInt(splitText[2]);
            if(id){
                const player = playerRep.getPlayerById(id);
                if(player){
                    const name = readline.question(`Enter new name(${player.name}): `);
                    const position = readline.question(`Position(${player.position}): `);
                    const age = getDate(`Date of birth(${player.age}): `);
                    let num = 0;
                    while(num <= 0){
                        num = getNum(`Number(${player.number}): `);
                    }
                    let titles = -1;
                    while(titles < 0){
                        titles = getNum(`All titles won(${player.titles}) - `);
                    }
                    const Newplayer = new Player(player.id, name, position, age, num, titles);
                    const success = playerRep.updatePlayer(Newplayer);
                    console.clear();
                    if(success){
                        console.log("Player was updated");
                        console.log(`${Newplayer.name}(${Newplayer.number}) - ${Newplayer.position}`);
                        console.log("Titles won - " + Newplayer.titles);
                        console.log("Date of birth - " + Newplayer.age);
                        console.log("");
                    }
                    else{
                        console.log("Fault");
                    }
                }
                else{
                    console.log("No player with such id");
                }
            }
            else{
                console.log("Id should be a number");
            }
        }
        else if(input === "post/player"){
            const name = readline.question("Enter the name: ");
            const position = readline.question("Position: ");
            const age = getDate("Date of birth: ");
            let num = 0;
            while(num <= 0){
                num = getNum("Number: ");
            }
            let titles = -1;
            while(titles < 0){
                titles = getNum("All titles won - ");
            }
            const player = new Player(0, name, position, age, num, titles);
            const id = playerRep.addPlayer(player);
            console.clear();
            console.log(name + "'s new id is " + id);
        }
        else if (input === "quit" || input === "exit"){
            break;
        }
        else{
            console.log("Wrong command");
        }
    }
}
catch(err){
    if(err.code === 'ENOENT'){
        console.log("No file or directory");
    }
    else if(err instanceof SyntaxError){
        console.log("Wrong file content");
    }
    else if(err.mesagge === "Wrong data format"){
        console.log("Wrong data format");
    }
    else{
        console.log("Uknown error: " + err.mesagge);
    }
}

function getNum(message){
    while(true){
        const numStr = readline.question(message);
        const num = parseInt(numStr);
        if(Number.isInteger(num)){
            return num;
        }
    }
}

function getDate(message){
    while(true){
        const dataStr = readline.question(message);
        const date = new Date(dataStr);
        if(date == "Invalid Date"){
            console.log("Wrong format");
            continue;
        }
        return date.toISOString();
    }
}