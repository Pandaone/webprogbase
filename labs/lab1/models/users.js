class User {

    constructor(id, login, fullname, role , registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role; // 0 or 1
        this.registeredAt = registeredAt; // ISO 8601
        this.avaUrl = avaUrl; // URL string
        this.isEnabled = isEnabled; // bool
    }
 };
 
 module.exports = User; 