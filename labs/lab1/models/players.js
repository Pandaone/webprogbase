class Player {

    constructor(id, name, position , age, number, titles) {
        this.id = id;  // number
        this.name = name;  // string
        this.position = position; //string
        this.age = age; // ISO 8601
        this.number = number; // number
        this.titles = titles; // number
    }
 };
 
 module.exports = Player; 