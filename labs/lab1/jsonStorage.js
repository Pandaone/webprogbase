class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const fs = require("fs");
        const text = fs.readFileSync(this.filePath).toString();
        const object = JSON.parse(text);
        const id = object.nextId;
        if(Number.isInteger(id)){
            return id;
        }
        return null;
    }

    incrementNextId() {
        let id = this.nextId;
        if(!id){
            throw new Error("Wrong data format");
        }
        const items = this.readItems();
        const user = {nextId : ++id, items : items};
        this.writeObjToFile(user);
    }

    readItems() {
        const fs = require("fs");
        const text = fs.readFileSync(this.filePath).toString();
        const object = JSON.parse(text);
        return object.items;
    }

    writeItems(items) {
        const id = this.nextId;
        if(!id){
            throw new Error("Wrong data format");
        }
        let user = {nextId : id, items : items};
        this.writeObjToFile(user);
    }

    writeObjToFile(obj){
        const fs = require("fs");
        const jsonText = JSON.stringify(obj, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }
};

module.exports = JsonStorage;