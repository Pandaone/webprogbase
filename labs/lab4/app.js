const config = require('./config.js');

const port = config.app.port;

const dbUrl = `${config.db.conn}${config.db.name}${config.db.params}`;
const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};

const express = require('express');

const app = express();

const morgan = require('morgan');
app.use(morgan('dev'));

app.use(express.static('public'));

const busboy = require('busboy-body-parser');
const optionsBusboy = {
   limit: '5mb',
   multi: false,
};
app.use(busboy(optionsBusboy));

const apiRouter = require('./routes/api.js');
app.use('', apiRouter);

const mustache = require('mustache-express');
const path = require('path');

const viewsDir = path.join(__dirname, 'public/views');
app.engine('mst', mustache(path.join(viewsDir, 'partials')));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.get('/', function(req, res) {
    res.render('index.mst');
});

const mongoose = require('mongoose');

app.listen(port, async function () { 
    console.log(`Server ready`);
    await mongoose.connect(dbUrl, connectOptions);
    console.log('Mongo database connected');
});