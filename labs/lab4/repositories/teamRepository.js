const TeamModel = require("../models/teams.js");

class TeamRepository {
    async getTeams() { 
        const teamsDocs = await TeamModel.find();
        const teams = teamsDocs.map(p => p.toObject());
        return teams;
    }
 
    async getTeamById(teamId) {
        const teamDoc = await TeamModel.findOne({_id: teamId});
        if(!teamDoc){
            return;
        }
        const team = teamDoc.toObject();
        return team;
    }

    async addTeam(teamModel) {
        await teamModel.save();
    }
 
    async updateTeam(teamModel) { 
        const team = await this.getPlayerById(teamModel._id);
        if(team){
            team = teamModel;
            await team.save();
            return true;
        }
        return false;
    }
 
    async deleteTeam(teamId) {
        const res = await TeamModel.deleteOne({ _id: teamId});
        if(res) return true;
        return false;
    }
};
 
module.exports = TeamRepository;