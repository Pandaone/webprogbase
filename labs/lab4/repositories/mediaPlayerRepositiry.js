const PlayersMediaModel = require('../models/playersMedia.js');

class mediaPlayerRepository {
    async addPlayerImage(playerImage){
        await playerImage.save();
    }

    async getImagesByPlayerId(playerId){
        const filesDocs = await PlayersMediaModel.find({player: playerId});
        const files = filesDocs.map(p => p.toObject());
        return files;
    }
}

module.exports = mediaPlayerRepository;