const UserModel = require("../models/users.js");
 
class UserRepository { 
    async getUsers() { 
        const usersDocs = await UserModel.find();
        const users = usersDocs.map(p => p.toObject());
        return users;
    }
 
    async getUserById(userId) {
        const userDoc = await UserModel.findOne({_id: userId});
        if(!userDoc){
            return;
        }
        const user = userDoc.toObject();
        return user;
    }

    async addUser(userModel) {
        await userModel.save();
    }
 
    async updateUser(userModel) {
        const user = await this.getPlayerById(userModel._id);
        if(user){
            user = user.save();
            return true;
        }
        return false;
    }
 
    async deleteUser(userId) {
        const user = await this.getPlayerById(userId);
        if(user){
            await user.remove();
            return true;
        }
        return false;
    }
};
 
module.exports = UserRepository;