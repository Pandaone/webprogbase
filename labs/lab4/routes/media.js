const router = require('express').Router();

const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const mediaController = require('../controllers/playersMedia.js');

router.post("/player", mediaController.postPlayerImage)
      .post("/download", urlencodedParser, mediaController.getImage);

module.exports = router;