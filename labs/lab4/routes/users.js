const router = require('express').Router();

const bodyParser = require('body-parser');

const userController = require('../controllers/users.js'); 

router.get("", userController.getUsers)
    .get("/:id([0-9a-f]+)", userController.getUserById)
    .post("", bodyParser.urlencoded({ extended: false }), userController.createUser)
    .get("/new", async function(req, res){
        res.render('new_user.mst');
    });
    


module.exports = router;