const userRouter = require("./users.js");
const playerRouter = require("./players.js");
const mediaRouter = require("./media.js");
const teamRouter = require("./teams.js");

const router = require('express').Router();

router.use("/users", userRouter)
    .use("/players", playerRouter)
    .use("/media", mediaRouter)
    .use("/teams", teamRouter);


router.get('/', async function(req, res) {
    res.status(200).render('index.mst');
});

router.get('/about', async function(req, res) {
    res.status(200).render('about.mst');
});

module.exports = router;