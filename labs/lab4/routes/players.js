const router = require('express').Router();

const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const playerController = require('../controllers/players.js');

router.get("/:id([0-9a-f]+)", playerController.getPlayerById)
    .get("", playerController.getAllPlayers)
    .post("", urlencodedParser, playerController.getPlayer, playerController.createPlayer)
  //.post("/update", urlencodedParser, playerController.getPlayer, playerController.putPlayer)
    .post("/delete", urlencodedParser, playerController.deletePlayer)
    .post("/new", urlencodedParser, playerController.renderCreationMenu);

module.exports = router;