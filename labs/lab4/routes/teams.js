const router = require('express').Router();

const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const teamController = require('../controllers/teams.js');

router.get("/:id([0-9a-f]+)", teamController.getTeamById)
    .get("", teamController.getAllTeams)
    .post("", urlencodedParser, teamController.getTeam, teamController.createTeam)
    .post("/new", urlencodedParser, teamController.renderCreationMenu);

module.exports = router;