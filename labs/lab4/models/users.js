const mongoose = require('mongoose');

const UserShema = new mongoose.Schema({
    login: {type: String, required: true},
    fullname: {type: String, required: true},
    role: {type: Number, default: 0},
    registeredAt: {type: Date, default: Date.now},
    avaUrl: {type: String, default: "https://res.cloudinary.com/duzykfess/image/upload/v1614769608/usersImages/user_eutk5f.png"},
    isEnabled: {type: Boolean, default: false}
});

module.exports = mongoose.model('Users', UserShema, 'Users');