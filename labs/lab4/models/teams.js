const mongoose = require('mongoose');

const PlayerShema = new mongoose.Schema({
    name: {type: String, required: true},
    stadium: {type: String, required: true},
    logo: {type: String, default: "https://res.cloudinary.com/duzykfess/image/upload/v1614794262/teamsImages/logo_xfdxl2.png"},
    desc: {type: String},
    user: {type: mongoose.mongo.ObjectId, ref: 'Users', required: true}
});

module.exports = mongoose.model('Teams', PlayerShema, 'Teams');