const mongoose = require('mongoose');

const PlayerMediaShema = new mongoose.Schema({
    player: {type: mongoose.mongo.ObjectId, ref: 'Players', required: true},
    name: {type: String, required: true}
});

module.exports = mongoose.model('PlayersMedia', PlayerMediaShema, 'PlayersMedia');