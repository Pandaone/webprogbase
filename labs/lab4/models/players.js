const mongoose = require('mongoose');

const PlayerShema = new mongoose.Schema({
    name: {type: String, required: true},
    team: {type: mongoose.mongo.ObjectId, ref: 'Teams', required: true},
    position: {type: String, required: true},
    age: {type: Date, required: true},
    number: {type: Number, required: true}
});

module.exports = mongoose.model('Players', PlayerShema, 'Players');