const userRepository = require("../repositories/userRepository.js");
const userRep = new userRepository();

const UserModel = require("../models/users.js");

module.exports = {
    async getUsers(req, res) {
        try{
            const allUsers = await userRep.getUsers();
        
            let users = [];
            const page = parseInt(req.query.page);
            const per_page = parseInt(req.query.per_page);
            for(let i = (page - 1) * per_page; i < page * per_page; i++){
                if(!allUsers[i]){
                    break;
                }
                users.push(allUsers[i]);
            }

            if(users.length === 0){
                if(page === 1){
                    const pages = {"cur_page": 1};
                    res.render('users.mst', {
                        users,
                        pages
                    });
                    return;
                }
                const responseErr = {
                    "Error": "Not found"
                };
                res.status(404).send(responseErr);
                return;
            }

            for(let user of users){
                user.registeredAt = user.registeredAt.toISOString().substring(0, 10);
            }

            const max_page = Math.ceil(allUsers.length / per_page);
            let next_page = null;
            if(max_page > page){
                next_page = page + 1;
            }
            let prev_page = null;
            if(page > 1){
                prev_page = page - 1;
            }
            const pages = {
                "prev_page": prev_page,
                "cur_page": page,
                "next_page": next_page
            };
            res.render('users.mst', {
                users,
                pages
            });
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getUserById(req, res) {
        try{
            const userId = req.params.id;
            const user = await userRep.getUserById(userId);
            if(user){
                user.registeredAt = user.registeredAt.toISOString().substring(0, 10);
                res.status(200).render("user.mst", {
                    user
                });
            }
            else{
                const response = {
                    "Error": "User not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async createUser(req, res) {
        try{
            const login = req.body.login;
            const fullname = req.body.fullname;
            if(!login || !fullname){
                const response = {
                    "Error": "Bad request"
                };
                res.status(400).send(response);
                return;
            }
            const user = new UserModel({
                login: login,
                fullname: fullname
            });
            const role = parseInt(req.body.role);
            if(role) user.role = role;
            const registeredAt = new Date(req.body.registeredAt);
            if(role) user.registeredAt = registeredAt;
            const avaUrl = req.body.avaUrl;
            if(role) user.avaUrl = avaUrl;

            await userRep.addUser(user);
            res.status(201);
            res.redirect(`/users/${user._id}`);
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    }
};
