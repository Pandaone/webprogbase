const teamRepository = require("../repositories/teamRepository.js");
const teamRep = new teamRepository();

const mediaRepository = require('../repositories/mediaRepository');
const mediaRep = new mediaRepository();

const TeamModel = require("../models/teams.js");

const teamSymbol = Symbol("team");
const imageSymbol = Symbol("image");

module.exports = {
    async getTeam(req, res, next){
        const name = req.body.name;
        const stadium = req.body.stadium;
        const user = req.body.user;

        if(!user || !stadium || !name){
            res.sendStatus(204);
            return;
        }
        const team = new TeamModel({
            name: name, 
            stadium: stadium,
            user: user
        });

        const logo = req.files.logo;
        req[imageSymbol] = logo;
        const desc = req.body.desc;
        if(desc){
            team.desc = desc;
        }
        
        req[teamSymbol] = team;
        next();
    },

    async getAllTeams(req, res) {
        try{
            const allTeams = await teamRep.getTeams();

            let teams = [];
            const page = parseInt(req.query.page);
            const per_page = parseInt(req.query.per_page);
            for(let i = (page - 1) * per_page; i < page * per_page; i++){
                if(!allTeams[i]){
                    break;
                }
                teams.push(allTeams[i]);
            }

            if(teams.length === 0){
                if(page === 1){
                    const pages = {"cur_page": 1};
                    res.render('teams.mst', {
                        teams,
                        pages
                    });
                    return;
                }
                const responseErr = {
                    "Error": "Not found"
                };
                res.status(404).send(responseErr);
                return;
            }
            
            const max_page = Math.ceil(allTeams.length / per_page);
            let next_page = null;
            if(max_page > page){
                next_page = page + 1;
            }
            let prev_page = null;
            if(page > 1){
                prev_page = page - 1;
            }
            const pages = {
                "prev_page": prev_page,
                "cur_page": page,
                "next_page": next_page
            };
            res.status(200).render('teams.mst', {
                teams,
                pages
            });
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getTeamById(req, res) {
        try{
            const teamId = req.params.id;
            const team = await teamRep.getTeamById(teamId);
            if(team){
                res.status(200).render("team.mst", {
                    team
                });
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }        
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async createTeam(req, res) {
        try{
            const team = req[teamSymbol];
            const logo = req[imageSymbol];
            if(logo){
                const imageData = logo.data;
                const res = await mediaRep.addImageToCloud(imageData, "teamsImages");
                team.logo = res.secure_url;
            }
            
            await team.save();        

            res.status(201);
            res.redirect(`/teams/${team._id}`);
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async renderCreationMenu(req, res){
        const id = req.body.id;
        res.render('new_team.mst', {
            id
        });
    }
};