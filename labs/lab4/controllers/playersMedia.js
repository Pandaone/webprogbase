const mediaPlayerRepository = require("../repositories/mediaPlayerRepositiry.js");
const mediaPlayerRep = new mediaPlayerRepository();

const mediaRepository = require("../repositories/mediaRepository.js");
const mediaRep = new mediaRepository();

const playerMediaModel = require("../models/playersMedia.js");

module.exports = {
    async postPlayerImage(req, res){
        try{
            const player_id = req.body.id;
            const image = req.files.image;
            if(image){
                const imageObj = await mediaRep.addImageToCloud(image.data, "playersImages");
                const playerImage = new playerMediaModel({
                    player: player_id,
                    name: imageObj.secure_url
                });
                await mediaPlayerRep.addPlayerImage(playerImage);
                res.status(201);
                res.redirect(`/players/${player_id}`);
            }
            else{
                res.sendStatus(204);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getImage(req, res){      
        const name = req.body.name;
        res.download(name);
    }
};