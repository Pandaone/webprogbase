const playerRepository = require("../repositories/playerRepository.js");
const playerRep = new playerRepository();

const PlayerModel = require("../models/players.js");

const playerSymbol = Symbol("player");

const mediaPlayerRepository = require("../repositories/mediaPlayerRepositiry.js");
const mediaPlayerRep = new mediaPlayerRepository();

module.exports = {
    async getPlayer(req, res, next){
        const name = req.body.name;
        const team = req.body.team;
        const position = req.body.position;
        const number = parseInt(req.body.number);
        const age = new Date(req.body.age);

        if(age == "Invalid Date" || !number || !position || !name || !team){
            res.sendStatus(204);
            return;
        }
        const player = new PlayerModel({
            name: name, 
            team: team,
            position: position,
            age: age,
            number: number
        });
        req[playerSymbol] = player;
        next();
    },

    async getAllPlayers(req, res) {
        try{
            const text = req.query.text.toLowerCase();
            const allPlayers = await playerRep.getPlayers(text);

            let players = [];
            const page = parseInt(req.query.page);
            const per_page = parseInt(req.query.per_page);
            for(let i = (page - 1) * per_page; i < page * per_page; i++){
                if(!allPlayers[i]){
                    break;
                }
                players.push(allPlayers[i]);
            }

            if(players.length === 0){
                if(page === 1){
                    const pages = {"cur_page": 1};
                    res.render('players.mst', {
                        players,
                        pages
                    });
                    return;
                }
                const responseErr = {
                    "Error": "Not found"
                };
                res.status(404).send(responseErr);
                return;
            }
            
            const max_page = Math.ceil(allPlayers.length / per_page);
            let next_page = null;
            if(max_page > page){
                next_page = page + 1;
            }
            let prev_page = null;
            if(page > 1){
                prev_page = page - 1;
            }
            const pages = {
                "prev_page": prev_page,
                "cur_page": page,
                "next_page": next_page
            };
            res.status(200).render('players.mst', {
                players,
                pages,
                text
            });
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async getPlayerById(req, res) {
        try{
            const playerId = req.params.id;
            const player = await playerRep.getPlayerById(playerId);
            if(player){
                player.age = player.age.toISOString().substring(0, 10);
                const images = await mediaPlayerRep.getImagesByPlayerId(playerId);
                
                res.status(200).render("player.mst", {
                    player,
                    images,
                });
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(500).send(response);
        }
    },

    async createPlayer(req, res) {
        try{
            const player = req[playerSymbol];
            await player.save();
            res.status(201);
            res.redirect(`/players/${player._id}`);
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async putPlayer(req, res){
        try{
            const player = req[playerSymbol];
            const success = await playerRep.updatePlayer(player);
            if(success){
                res.redirect(`/players/${player._id}`);
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async deletePlayer(req, res){
        try{
            const playerId = req.body.id;
            const player = await playerRep.deletePlayer(playerId);
            if(player){
                res.redirect("/players?page=1&per_page=4&text=");
            }
            else{
                const response = {
                    "Error": "Not found"
                };
                res.status(404).send(response);
            }
        }
        catch(err){
            const response = {
                "Error": "Something got wrong"
            };
            res.status(502).send(response);
        }
    },

    async renderCreationMenu(req, res){
        const id = req.body.id;
        console.log(id);
        res.render('new_player.mst', {
            id
        });
    }
};